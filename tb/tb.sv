`timescale 1ns/1ns

module test;
  logic  clk, start, dec;
  logic  [3:0] in;
  logic stop;
  // Instantiate device under test
  Counter counter(.clk(clk),
          .start(start),
          .in(in),
          .dec(dec),
          .stop(stop));
  always
    begin
      #5 clk =~clk;
    end

  initial begin
    $dumpfile("counter.vcd");
    $dumpvars(0,test);
    clk = 0;
    in = 0;
    start = 0;
    dec = 0;
    #5;
    in = 3;
    start = 1;
    #10;
    start = 0;
    dec = 1;
    #10;
    dec = 1;
    #10;
    dec = 1;
    #20;
    $finish;
  end
endmodule

