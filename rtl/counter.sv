








module Counter (clk,start,in,dec,stop);
input clk,start,dec;
input [3:0] in;
output stop;
logic [3:0] value;

always_ff @(posedge clk) begin 
    if (start) begin
        value<=in;
    end else if(dec && !stop) begin
        value<=value-1'b1;
    end
end 

assign stop = (value==4'b0);
endmodule
